#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class fatjet+;
#pragma link C++ class truthfatjet+;
#pragma link C++ class trackjet+;
#pragma link C++ class muon+;
#pragma link C++ class Nominal+;

#endif
